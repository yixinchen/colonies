clearvars;
path = './data/NewMicroscopePhoto/';
%fid1 = fopen('./data/filelist1.txt');
fid1 = fopen('./data/filelist2.txt');
while 1
    fname = fgetl(fid1);
    if ~ischar(fname), break, end
    disp(fname)
    fid2 = fopen(strcat(path,strcat(fname,'.txt')), 'w');
    %colonies = findcolonies(strcat(path,fname),'radiibound',[70 300]);
    colonies = findcolonies(strcat(path,fname),'radiibound',[100 300],'sensitivity',0.97);
    fprintf(fid2, '%10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n',...
        'col1', 'row1', 'r1', 'v1', 'col2', 'row2', 'r2', 'v2', 'bcol', 'brow', 'br');
    for i = 1:length(colonies)
        fprintf(fid2, '%10.4f %10.4f %10d %10d %10.4f %10.4f %10d %10d %10.4f %10.4f %10.4f\n',...
            colonies(i).center1(1), colonies(i).center1(2),...
            colonies(i).radius1, colonies(i).medval1,...
            colonies(i).center2(1), colonies(i).center2(2),...
            colonies(i).radius2, colonies(i).medval2,...
            colonies(i).bigcenter(1), colonies(i).bigcenter(2),...
            colonies(i).bigradius);
    end
    fclose(fid2);
end
fclose(fid1);